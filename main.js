import createFastify from 'fastify';
import fastifyCors from '@fastify/cors';
import {
    getProfile,
    getPost, getPartialProfile, getPartialPost
} from 'scraper-instagram';
import jwt from 'jsonwebtoken';
import axios from 'axios';

import {
    jwtSecret,
    host
} from './config.js';

const
    fastify = createFastify({
        maxParamLength: 1000
    }),
    port = parseInt(process.env.PORT),
    assetUrlToTokenUrl = url => `${host}/assets/${jwt.sign({ url }, jwtSecret)}`,
    assetTokenToUrl = token => (jwt.verify(token, jwtSecret)).url;

fastify.register(fastifyCors);

fastify.addHook(
    'onError',
    (
        request,
        reply,
        error,
        done
    ) => {
        console.error(error);
        done();
    }
);

fastify.get(
    '/partialProfile/:username',
    async (
        request,
        reply
    ) => {
        const result = await getPartialProfile({ username: request.params['username'] });
        result['pictureUrl'] = assetUrlToTokenUrl(result['pictureUrl']);
        reply.send(result);
    }
);

fastify.get(
    '/profile/:username',
    async (
        request,
        reply
    ) => {
        const result = await getProfile({ username: request.params['username'] });
        result['pictureUrl'] = assetUrlToTokenUrl(result['pictureUrl']);
        for(const post of result['posts'])
            post['thumbnailUrl'] = assetUrlToTokenUrl(post['thumbnailUrl']);
        for(const profile of result['relatedProfiles'])
            profile['pictureUrl'] = assetUrlToTokenUrl(profile['pictureUrl']);
        reply.send(result);
    }
);

fastify.get(
    '/partialPost/:shortcode',
    async (
        request,
        reply
    ) => {
        const result = await getPartialPost({ shortcode: request.params['shortcode'] });
        result['thumbnailUrl'] = assetUrlToTokenUrl(result['thumbnailUrl']);
        reply.send(result);
    }
);

fastify.get(
    '/post/:shortcode',
    async (
        request,
        reply
    ) => {
        const result = await getPost({ shortcode: request.params['shortcode'] });
        result['thumbnailUrl'] = assetUrlToTokenUrl(result['thumbnailUrl']);
        result['author']['pictureUrl'] = assetUrlToTokenUrl(result['author']['pictureUrl']);
        for(const item of result['contents']){
            item['url'] = assetUrlToTokenUrl(item['url']);
            if(item['thumbnailUrl'])
                item['thumbnailUrl'] = assetUrlToTokenUrl(item['thumbnailUrl']);
            if(item['taggedProfiles']){
                for(const profile of item['taggedProfiles'])
                    profile['pictureUrl'] = assetUrlToTokenUrl(profile['pictureUrl']);
            }
        }
        reply.send(result);
    }
);

fastify.get(
    '/assets/:token',
    async (
        request,
        reply
    ) => {
        reply.send((await axios({
            url: assetTokenToUrl(request.params['token']),
            responseType: 'arraybuffer'
        })).data);
    }
);

fastify
    .listen({ port })
    .then(() => console.log(`Server listening on port ${port}`))
    .catch(console.error);